#include<stdio.h>
#include<curl\curl.h>
#include <io.h>  
#include<string>
#include<iostream>
#include<vector>
#include <boost/regex.hpp>
#include<fstream>


using namespace std;

vector<string> URL_Vec;


//这是libcurl接收数据的回调函数，相当于recv的死循环  
//其中stream可以自定义数据类型，这里我传入的是文件保存路径  
static size_t write_callback( void *ptr, size_t size, size_t nmemb, void *stream )  
{  
	int len = size * nmemb;  
	int written = len;  
	FILE *fp = NULL;  

	if ( access( (char*)stream, 0 ) == -1 )  
	{  
		fp = fopen( (char*) stream, "wb" );  
	}  
	else  
	{  
		fp = fopen( (char*) stream, "ab" );  
	}  
	if (fp)  
	{  
		fwrite( ptr, size, nmemb, fp );  
	}  
	fclose(fp);
	return written;  
}  

int GetUrl( const char* url, const char *savepath )  
{  
	CURL *curl;  
	CURLcode res;  
	struct curl_slist *chunk = NULL;  
	curl_global_init(CURL_GLOBAL_ALL);
	curl = curl_easy_init();  
	if ( curl ) {  
		curl_easy_setopt( curl, CURLOPT_VERBOSE, 0L );
		curl_easy_setopt( curl, CURLOPT_URL, url );
		//指定回调函数  
		curl_easy_setopt( curl, CURLOPT_WRITEFUNCTION, write_callback);
		//这个变量可作为接收或传递数据的作用  
		curl_easy_setopt( curl, CURLOPT_WRITEDATA, savepath );
		res = curl_easy_perform( curl );
		if (res == CURLE_OK)  
		{  
			return 1;  
		}  
		return 0;  
	}
}

int Analyse_ITmop(const char* filename){
	//char buf[1027]={0};
	string str;
	bool find_flag = false;
	bool find_atlease = false;
	const string str_itmop_begin("<DT><a href=\"");
	int int_itmop_begin(0);
	int int_itmop_end(0);
	ifstream fin;
	fin.open(filename);

	const string str_google_end("\" target=");
	//FILE * fp = fopen(filename,"r");
	while(getline(fin,str)){
		//str = buf;
		if((int_itmop_begin = str.find(str_itmop_begin))!=-1){
			find_flag = true;
			find_atlease = true;
			if(find_flag == true&&(int_itmop_end = str.find(str_google_end,int_itmop_begin+13))!=-1){
				find_flag = false;
				string str_url = str.substr(int_itmop_begin+13,int_itmop_end-int_itmop_begin-13);
				URL_Vec.push_back(str_url);
				//cout<<str_url<<endl;
			}
		}

	}
	fin.clear();
	fin.close();
	FILE *fp = fopen(filename,"w");
	fclose(fp);


	if(!find_atlease){
		return 0;
	}else{
		return 1;
	}
}

void Analyse_Proxy(const char* path)
{    
	const char *szReg = "((\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3}):(\\d{1,5}))";
	//char buf[1027]={0};
	string str;
	ifstream fin;
	fin.open(path);
	//int ii = 0;
	while(getline(fin,str)){

		
		const char* szStr = str.c_str();


		//使用迭代器找出所有ip:port

		boost::regex reg(szReg );   
		boost::cregex_iterator itrBegin = make_regex_iterator(szStr,reg); //(szStr, szStr+strlen(szStr), reg);
		boost::cregex_iterator itrEnd;
		for(boost::cregex_iterator itr=itrBegin; itr!=itrEnd; ++itr)
		{
			//子串内容
			cout <<*itr << endl;
		}
		//ii++;
	}
	//cout<<ii<<endl;
	fin.clear();
	fin.close();
	FILE *fp=fopen(path,"w");
	fclose(fp);
}

void main(int argc,char* argv[])  
{  
	int page;
	const char* ctempfile_name;

	if(!(argc==1)){
		page = atoi(argv[1]);
		string process = argv[2];
		string tempfile_name = process+".xml";
		ctempfile_name = tempfile_name.c_str();

		char buf[3] = {0};

		itoa(page,buf,10);
		string aft=buf;
		string url = "http://www.itmop.com/proxy/catalog.asp?page="+aft;
		const char* cu = url.c_str();
		if ( GetUrl(cu, ctempfile_name ) )  
		{  
			Analyse_ITmop(ctempfile_name);
			if(0!=URL_Vec.size()){
				for(int i=0;i<URL_Vec.size();i++){
					if(GetUrl(URL_Vec[i].c_str(),ctempfile_name)){
						Analyse_Proxy(ctempfile_name);
					}

				}
				URL_Vec.clear();
			}
		}
		else
			cout<<"couldn't download html files"<<endl;
		//return 0;  

	}else{
		ctempfile_name = "1.xml"; 
		page = 1;
		char buf[3] = {0};

		itoa(page,buf,10);
		string aft=buf;
		string url = "http://www.itmop.com/proxy/catalog.asp?page="+aft;
		const char* cu = url.c_str();
		if ( GetUrl(cu, ctempfile_name ) )  
		{  
			Analyse_ITmop(ctempfile_name);
			if(0!=URL_Vec.size()){
				for(int i=0;i<URL_Vec.size();i++){
					if(GetUrl(URL_Vec[i].c_str(),ctempfile_name)){
						Analyse_Proxy(ctempfile_name);
					}

				}
				URL_Vec.clear();
			}
		}
		else
			cout<<"couldn't download html files"<<endl;
		//return 0;  
	}
}

