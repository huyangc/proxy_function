#include<stdio.h>
#include<curl\curl.h>
#include <io.h>  
#include<string>
#include<iostream>
#include<vector>
#include<fstream>
#include <boost/regex.hpp>

#define GOOGLESEARCHBEF "http://www.google.com.hk/search?num=20&q="
#define GOOGLESEARCHBEF2 "http://www.google.com.hk/search?&q="
#define PAGE "&start="
using namespace std;
bool outofrange;
vector<string> URL_Vec;


//这是libcurl接收数据的回调函数，相当于recv的死循环  
//其中stream可以自定义数据类型，这里我传入的是文件保存路径  
static size_t write_callback( void *ptr, size_t size, size_t nmemb, void *stream )  
{  
	int len = size * nmemb;  
	int written = len;  
	FILE *fp = NULL;  

	if ( access( (char*)stream, 0 ) == -1 )  
	{  
		fp = fopen( (char*) stream, "wb" );  
	}  
	else  
	{  
		fp = fopen( (char*) stream, "ab" );  
	}  
	if (fp)  
	{  
		fwrite( ptr, size, nmemb, fp );  
	}  
	fclose(fp);
	return written;  
}  

int GetUrl( const char* url, const char *savepath ,const char* cookiefile)  
{  
	CURL *curl;  
	CURLcode res;  
	struct curl_slist *chunk = NULL;  
	curl_global_init(CURL_GLOBAL_ALL);
	curl = curl_easy_init();  
	if ( curl ) {  
		curl_easy_setopt( curl, CURLOPT_VERBOSE, 0L );
		curl_easy_setopt( curl, CURLOPT_URL, url );
		//指定回调函数  
		curl_easy_setopt( curl, CURLOPT_WRITEFUNCTION, write_callback);
		//这个变量可作为接收或传递数据的作用  
		curl_easy_setopt(curl,CURLOPT_COOKIEFILE,cookiefile);
		curl_easy_setopt(curl,CURLOPT_TCP_KEEPALIVE,0);
		//curl_easy_setopt(curl,CURLOPT_COOKIEJAR,cookiefile);
		curl_easy_setopt( curl, CURLOPT_WRITEDATA, savepath );
		
		res = curl_easy_perform( curl );
		if (res == CURLE_OK)  
		{  
			curl_easy_cleanup(curl);
			//cout<<"download succeed"<<endl;
			return 1;  
		}else
		{
			//cout<<"download fail"<<endl;
			return 0; 
		}
	}else
		return 0;
}

int Analyse_Google_Url(const char* filename){
	//char buf[1027]={0};
	//cout<<"in analyse"<<endl;
	string str;
	int find_flag = 0;
	bool find_atlease = false;
	const string str_google_begin("<h3 class=\"r\"><a href=\"/url?q=");
	int int_google_begin(0);
	int int_google_end(0);
	
	const string str_google_end("&amp");
	//const char* regex_url = "<h3 class=\"r\"><a href=\"/url?q=([\\s\\S]*)&amp";
	ifstream fin;
	fin.open(filename);
	const char * szStr;

	while(getline(fin,str))
	{
		if(str.find("找不到和您的查询")!=-1){
			outofrange = true;
			break;
		}
		//cout<<str<<endl;
		while((int_google_begin = str.find(str_google_begin,find_flag))!=-1)
		{
		find_flag = int_google_begin;
		find_atlease = true;
		if(find_flag != 0&&(int_google_end = str.find(str_google_end,find_flag))!=-1)
		{
		find_flag = int_google_end;
		string str_url = str.substr(int_google_begin+30,int_google_end-int_google_begin-30);
		URL_Vec.push_back(str_url);
		}
		}
		/*const char* szStr = str.c_str();


		//使用迭代器找出所有ip:port

		boost::regex reg(regex_url );   
		boost::cregex_iterator itrBegin = make_regex_iterator(szStr,reg); //(szStr, szStr+strlen(szStr), reg);
		boost::cregex_iterator itrEnd;
		for(boost::cregex_iterator itr=itrBegin; itr!=itrEnd; ++itr)
		{
			//子串内容
			cout <<*itr << endl;
		}
		*/
	}

	FILE *fp = fopen(filename,"w");
	fin.clear();
	fin.close();
	fclose(fp);


	if(!find_atlease)
	{
		return 0;
	}else
	{
		return 1;
	}
}

void Analyse_Proxy(const char* path)
{    
	const char *szReg = "((\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5]):(\\d{1,5}))";
	
	string str;
	
	ifstream fin;
	fin.open(path);
	
	while(getline(fin,str))
	{
		
		const char* szStr = str.c_str();


		//使用迭代器找出所有ip:port

		boost::regex reg(szReg );   
		boost::cregex_iterator itrBegin = make_regex_iterator(szStr,reg); //(szStr, szStr+strlen(szStr), reg);
		boost::cregex_iterator itrEnd;
		for(boost::cregex_iterator itr=itrBegin; itr!=itrEnd; ++itr)
		{
			//子串内容
			cout <<*itr << endl;
		}
		
	}
	
	FILE *fp=fopen(path,"w");
	fin.close();
	fin.clear();
	fclose(fp);
}

void main(int argc,char* argv[])  
{  
	string flag_searchtype = argv[1];
	string key_word = argv[2];
	string page = argv[3];
	string process = argv[4];
	string CookieFilePath = argv[5];
	string url;
	if(flag_searchtype == "0")
		url = GOOGLESEARCHBEF+key_word+PAGE+page+"0";
	else
		url = GOOGLESEARCHBEF2+key_word+PAGE+page+"0";
	string tempfile_name = process+".xml";
	char* ctempfile_name = const_cast<char*>(tempfile_name.c_str());
	const char* curl = url.c_str();
	

	
	if ( GetUrl(curl, ctempfile_name,CookieFilePath.c_str() ) )  
	{  
		
		Analyse_Google_Url(ctempfile_name);
		if(0!=URL_Vec.size())
		{
			if(flag_searchtype == "0")
			{
				for(int i=0;i<URL_Vec.size();i++)
				{
					cout<<URL_Vec[i]<<endl;
				}
			}
			else if(flag_searchtype == "1")
			{
				for(int i=0;i<URL_Vec.size();i++)
				{
					if(GetUrl(URL_Vec[i].c_str(),ctempfile_name,CookieFilePath.c_str()))
					{
						Analyse_Proxy(ctempfile_name);
					}
				}
			}
		}else{
			if(outofrange)
			
				cout<<"Page is too large"<<endl;
			else
				cout<<"need to use proxy"<<endl;
		}
	}else
		cout<<"couldn't download html files"<<endl;
} 