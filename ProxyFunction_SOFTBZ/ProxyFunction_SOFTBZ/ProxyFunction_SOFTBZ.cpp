#include<stdio.h>
#include<curl\curl.h>
#include <io.h>  
#include<string>
#include<iostream>
#include<vector>
#include <boost/regex.hpp>
#include<atlstr.h>
#include<fstream>


using namespace std;

vector<string> URL_Vec;


//这是libcurl接收数据的回调函数，相当于recv的死循环  
//其中stream可以自定义数据类型，这里我传入的是文件保存路径  
static size_t write_callback( void *ptr, size_t size, size_t nmemb, void *stream )  
{  
	int len = size * nmemb;  
	int written = len;  
	FILE *fp = NULL;  

	if ( access( (char*)stream, 0 ) == -1 )  
	{  
		fp = fopen( (char*) stream, "wb" );  
	}  
	else  
	{  
		fp = fopen( (char*) stream, "ab" );  
	}  
	if (fp)  
	{  
		fwrite( ptr, size, nmemb, fp );  
	}  
	fclose(fp);
	return written;  
}  

int GetUrl( const char* url, const char *savepath )  
{  
	CURL *curl;  
	CURLcode res;  
	struct curl_slist *chunk = NULL;  
	curl_global_init(CURL_GLOBAL_ALL);
	curl = curl_easy_init();  
	if ( curl ) {  
		curl_easy_setopt( curl, CURLOPT_VERBOSE, 0L );
		curl_easy_setopt( curl, CURLOPT_URL, url );
		//指定回调函数  
		curl_easy_setopt( curl, CURLOPT_WRITEFUNCTION, write_callback);
		//这个变量可作为接收或传递数据的作用  
		curl_easy_setopt( curl, CURLOPT_WRITEDATA, savepath );
		res = curl_easy_perform( curl );
		if (res == CURLE_OK)  
		{  
			curl_easy_cleanup(curl);
			return 1;  
		}  
		curl_easy_cleanup(curl);
		return 0;  
	}
	FILE *fc=fopen(savepath,"r");
	fclose(fc);
}

void Analyse_Proxy(const char* path)
{    
	const char *szReg = "((\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3}):(\\d{1,5}))";
	//char buf[1027]={0};
	string str;
	//FILE *fp=fopen(path,"r");
	ifstream fin;
	fin.open(path);
	int ii = 0;
	while(getline(fin,str)){

		
		const char* szStr = str.c_str();


		//使用迭代器找出所有ip:port

		boost::regex reg(szReg );   
		boost::cregex_iterator itrBegin = make_regex_iterator(szStr,reg); //(szStr, szStr+strlen(szStr), reg);
		boost::cregex_iterator itrEnd;
		for(boost::cregex_iterator itr=itrBegin; itr!=itrEnd; ++itr)
		{
			//子串内容
			cout <<*itr << endl;
		}
		//ii++;
	}
	//cout<<ii<<endl;
	fin.clear();
	FILE *fc=fopen(path,"w");
	fclose(fc);
	//CString dir = path;
	//SetFileAttributes(dir, FILE_ATTRIBUTE_NORMAL);
	//DeleteFile(dir);
	
	
}

void main(int argc,char* argv[])  
{  
	string url = "http://soft.bz/razdacha-proxy/proxy-list-http-socks4-socks5-18-03-13/";
	const char* cu = url.c_str();
	const char* ctempfile_name;

	if(!(argc==1)){
		string process = argv[1];
		string tempfile_name = process+".xml";
		ctempfile_name = tempfile_name.c_str();
		if ( GetUrl(cu, ctempfile_name ) )  
		{  
			Analyse_Proxy(ctempfile_name);
		}
		else
			cout<<"couldn't download html files"<<endl;
		//return 0;  
	}else{
		string tempfile_name = "1.xml";
		ctempfile_name = tempfile_name.c_str();
		if ( GetUrl(cu, ctempfile_name ) )  
		{  
			Analyse_Proxy(ctempfile_name);
		}
		else
			cout<<"couldn't download html files"<<endl;
		//return 0;  
	}
	
}